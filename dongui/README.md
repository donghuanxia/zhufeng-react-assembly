This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).




2.修改 package.json

name为 @用户名/原包名
把仓库上传到github,并把地址贴到repository里,仓库的名字和包名是一样的
registry为 https://npm.pkg.github.com/用户名
{
+  "name": "@zhufengnodejs/zhufengreactui",
+  "repository":"https://github.com/zhufengnodejs/zhufengreactui",
  "publishConfig": {
+    "registry": "https://npm.pkg.github.com/zhufengnodejs"
  }
3.登录npm

npm login --registry=https://npm.pkg.github.com --scope=@zhufengnodejs
username 为你的github用户名
password 为刚才第一步生成的token


使用作用域名进行登录：

npm login --registry=https://npm.pkg.github.com --scope=@yehuozhili
会让你输入密码，这个密码是githubtoken,注意，是githubtoken ,是githubtoken。重要的事说3遍，因为这个设计有点反人类，它是粘贴并且看不见粘贴上没有。

githubtoken在github的setting里面生